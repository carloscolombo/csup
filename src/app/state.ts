import * as fs from 'fs';
import * as path from 'path'
import * as electron from 'electron'
import { BehaviorSubject } from 'rxjs';
import { CapturedInputs } from './game/game.component';

export interface State {
    users: {
        [id: string]: {
            name: string;
            procedures: string;
            results?: Results;
        }
    }
}

export interface AddUser {
    id: string;
    name: string;
    procedures: string;
    delete: boolean
}

export interface Results {
    userId: string;
    startTime: number;
    endTime: number;
    caputredInputs: CapturedInputs[]
}

export class AppState {
    public static state = new BehaviorSubject<State>(null);
    // public static state = null;

    public static create() {
        let parentPath = electron.remote.app.getAppPath();
        const statePath = `${parentPath}/main.json`;
        console.log(statePath);
        try {
            if (fs.existsSync(statePath)) {
                fs.readFile(statePath, (err, data) => {
                    console.log('File Read!')
                    AppState.state.next(JSON.parse(data.toString()));
                });

            } else {
                AppState.state.next({
                    users: {}
                })

                AppState.save();

            }
        } catch (error) {
            console.log(error);
        }
    }

    public static updateUsers(users: AddUser[]) {
        let state = AppState.state.value;

        users.forEach(x => {
            let user = state.users[x.id];

            if (user) {
                if (x.delete) {
                    delete state.users[x.id];
                } else {
                    user.name = x.name;
                    user.procedures = x.procedures;
                }
            }
            else {
                state.users[x.id] = {
                    name: x.name,
                    procedures: x.procedures,
                    results: {
                        caputredInputs: [],
                        endTime: 0,
                        startTime: 0,
                        userId: ''
                    }
                };
            }
        });

        AppState.state.next(state);
        AppState.save();
    }

    public static save() {
        let parentPath = electron.remote.app.getAppPath();
        const statePath = `${parentPath}/main.json`;

        fs.writeFile(statePath, JSON.stringify(AppState.state.value), (err) => {
            if (err) throw err;
            console.log('File Updated!')
        });
    }

    public static updateUserResults(userId: string, result: Results) {
        let state = AppState.state.value;
        let user = state.users[userId];

        if (user) {
            result.caputredInputs = result.caputredInputs.concat(user.results.caputredInputs);

            user.results = result;
            AppState.state.next(state);
            AppState.save();
        } else {
            alert('Unable to save the results!');
        }
    }

}