import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '../state';
import { Subscription } from 'rxjs/internal/Subscription';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'instructions',
  templateUrl: './instructions.component.html',
  styleUrls: ['./instructions.component.scss']
})
export class InstructionsComponent implements OnInit {
  stateSubscription: Subscription;
  phase = 'a';

  participants = [];
  form: FormGroup;

  constructor(private _router: Router, private _formBuilder: FormBuilder) { }

  ngOnInit() {
    this.form = this._formBuilder.group({
      participantCode: ''
    });

    this.stateSubscription = AppState.state.subscribe(state => {
      if (state) {
        this.participants = Object.keys(state.users)
          .map(x => {
            return {
              participantCode: state.users[x].name,
              id: x
            }
          });
      }
    });
  }

  startGame() {
    let user = this.participants.find(x => x.participantCode == this.form.value.participantCode);
    if (user)
      this._router.navigate(['/game'], {
        queryParams: {
          t: new Date().getTime(),
          p: user.id
        }
      })
    else {
      alert(`Participant Code '${this.form.value.participantCode}' not found`);
    }
  }

  phaseChange(value: string) {
    this.phase = value;
  }

  ngOnDestroy() {
    if (this.stateSubscription) this.stateSubscription.unsubscribe();
  }
}
