import { Component, OnInit, NgZone } from '@angular/core';
import { AppState } from "../state";
import { v4 as uuid } from 'uuid';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  form: FormGroup;
  users = [];

  stateSubscription: Subscription;

  constructor(private _formBuilder: FormBuilder, private _zone: NgZone) { }

  get formUsers() { return <FormArray>this.form.get('users') }

  ngOnInit() {
    this.form = this._formBuilder.group({
      users: this._formBuilder.array([])
    });

    this.stateSubscription = AppState.state.subscribe(state => {
      if (state) {
        console.log(state)
        this._zone.run(() => {
          let users = this.form.controls['users'] as FormArray;
          users.clear();

          Object.keys(state.users)
            .forEach(x => {
              let users = this.form.controls['users'] as FormArray;
              users.push(
                this._formBuilder.group({
                  id: x,
                  name: state.users[x].name,
                  procedures: state.users[x].procedures,
                  delete: false
                })
              );
            });
        });
      }
    });
  }

  addParticipant() {
    let users = this.form.controls['users'] as FormArray;
    users.push(
      this._formBuilder.group({
        id: uuid(),
        name: '',
        procedures: '',
        delete: false
      })
    );
  }

  removeParticipant(index: number) {
    let users = this.form.controls['users'] as FormArray;
    let user = users.controls[index] as FormGroup;

    if (user) {
      user.controls['delete'].setValue(!user.controls['delete'].value)
    }
  }

  save() {
    AppState.updateUsers(this.form.value.users);
    alert('Users Updated!')
  }

  ngOnDestroy() {
    if (this.stateSubscription) this.stateSubscription.unsubscribe();
  }

}
