import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';

import { HomeComponent } from './home.component';
import { SharedModule } from '../shared/shared.module';

import { InstructionsComponent } from '../instructions/instructions.component'
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  declarations: [HomeComponent, InstructionsComponent],
  imports: [CommonModule, SharedModule, HomeRoutingModule, ReactiveFormsModule, FormsModule]
})
export class HomeModule { }
