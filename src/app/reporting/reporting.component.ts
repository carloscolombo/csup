import { Component, OnInit } from '@angular/core';
import * as XLSX from 'xlsx';
import * as electron from 'electron';
import { AppState } from '../state';

@Component({
  selector: 'app-reporting',
  templateUrl: './reporting.component.html',
  styleUrls: ['./reporting.component.scss']
})
export class ReportingComponent implements OnInit {
  formattedData = [];
  // wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'array' };
  fileName: string = 'SunkCostResults.xlsx';

  constructor() { }

  ngOnInit() {
    this.formatData();
    this.export();
  }

  formatData() {
    let state = AppState.state.value;

    Object.keys(state.users)
      .forEach(userId => {
        let user = state.users[userId];
        this.formattedData.push(["Participant Code", user.name]);
        this.formattedData.push(["Procedure", user.procedures]);
        this.formattedData.push([]);
        this.formattedData.push([]);
        this.formattedData.push(["", "Number of Correct Responses", "Score Obtained", "Duration of time played"]);

        if (user.results) {
          let groupIds = user.results.caputredInputs
            .map(x => x.groupId)
            .filter((value, index, self) => {
              return self.indexOf(value) === index;
            });

          groupIds.forEach(x => {
            let groupResults = user.results.caputredInputs.filter(y => y.groupId == x);

            if (groupResults.length > 0) {
              let phase = groupResults[0].phase;
              let correctResponses = groupResults
                .filter(y => y.isCorrect)
                .map(y => y.score);

              let score = correctResponses
                .reduce((p, c) => p + c);

              let phaseStartTime = new Date(groupResults[0].phaseStartTime);
              let phaseEndTime = new Date(groupResults[groupResults.length - 1].phaseEndTime);

              let totalTime = Math.abs(new Date(phaseStartTime).getTime() - new Date(phaseEndTime).getTime()) / 1000;
              let timeText = this.toTime(totalTime);

              this.formattedData.push([phase, correctResponses.length, score, timeText])
            }
          });

        }

        this.formattedData.push([]);
        this.formattedData.push([]);
      });
  }

  export(): void {
    let dialog = electron.remote.dialog;

    // let o = dialog.showOpenDialog({ properties: ['openFile'] });
    // let workbook = XLSX.readFile(o[0]);

    /* generate worksheet */
    const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(this.formattedData);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();

    XLSX.utils.book_append_sheet(wb, ws, 'DATA');

    // this._constructParticipantSheet(wb);
    let saveDialog = dialog.showSaveDialog({
      title: "Export Report",
      defaultPath: `csup - ${new Date().toDateString()}.xlsx`
    }).then((value) => {
      /* save to file */
      XLSX.writeFile(wb, value.filePath, {});
    });
  }

  // private _constructParticipantSheet(wb) {
  //   this.data.forEach(item => {
  //     let data = [];
  //     let fr10 = this._addValueToRound(item.a.r1.rounds, item.a.r1.value);
  //     let fr40 = this._addValueToRound(item.a.r2.rounds, item.a.r2.value);
  //     let fr80 = this._addValueToRound(item.a.r3.rounds, item.a.r3.value);
  //     let fr160 = this._addValueToRound(item.a.r4.rounds, item.a.r4.value);

  //     let rounds = [...fr10, ...fr40, ...fr80, ...fr160];

  //     for (let i = 1; i < item.trial + 1; i++) {
  //       let responses = [];

  //       rounds = rounds.sort((a, b) => +new Date(a.startDate) - +new Date(b.startDate));

  //       let startDate = new Date(rounds[0].startDate);

  //       rounds
  //         .filter(x => x.trial == i)
  //         .forEach(x => {
  //           responses = responses.concat(x.responses);
  //         });

  //       responses
  //         .sort((a, b) => +new Date(a.date) - +new Date(b.date))
  //         .forEach((x, i, arr) => {
  //           let results: any = [`FR${x.value}`];

  //           if (i == 0) {
  //             let seconds = (+new Date(x.date) - +startDate) / 1000;
  //             results.push(seconds + 's');
  //           }
  //           else {
  //             let seconds = (+new Date(x.date) - +new Date(arr[i - 1].date)) / 1000;
  //             results.push(seconds + 's');
  //           }
  //           results.push(x.type);

  //           data.push(results);
  //         });
  //     }

  //     const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(data);
  //     XLSX.utils.book_append_sheet(wb, ws, item.participant);
  //   });
  // }

  // private _addValueToRound(rounds, value) {
  //   rounds.forEach(x => {
  //     if (!x.responses) x.responses = [];
  //     x.responses.forEach(y => y['value'] = value)
  //   });

  //   return rounds;
  // }

  toTime(timeInSeconds) {
    let pad = function (num, size) { return ('000' + num).slice(size * -1); },
      time: any = parseFloat(timeInSeconds).toFixed(3),
      hours = Math.floor(time / 60 / 60),
      minutes = Math.floor(time / 60) % 60,
      seconds = Math.floor(time - minutes * 60);
    // milliseconds = time.slice(-3);

    return pad(hours, 2) + ':' + pad(minutes, 2) + ':' + pad(seconds, 2);
  }

}
