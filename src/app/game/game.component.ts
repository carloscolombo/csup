import { Component, OnInit, HostListener, Input } from '@angular/core';
import * as PIXI from 'pixi.js';
import { Router, ActivatedRoute } from '@angular/router';
import { MovingRect } from '../core/models';
import { AppState } from '../state';
import { v4 as uuid } from 'uuid';
// import { MovingRect } from '../core/models';

@Component({
  selector: 'game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {
  private _spacePress = false;
  private _isPaused = false;
  private _userId: string;
  private _phaseSequence: string[] = []

  private _h: number = window.innerHeight;
  private _w: number = window.innerWidth;

  private _sessionStartTime: number;
  private _sessionEndTime: number;
  private _phaseStartTime: number;
  private _phaseEndTime: number;

  private _renderer;
  private _stage: PIXI.Container;

  private _stripHeight = 200;

  private _strip: PIXI.Graphics;

  private _arrow: PIXI.Graphics;
  private _score = 0;
  private _scoreModifier = 5;
  private _showScore = true;
  private _scoreText: PIXI.Text;
  private _rectQueue: MovingRect[] = [];
  private _movingRects: MovingRect[] = [];
  private _movingRectMaxWidth = 200;
  private _rectDistance = 0;
  private _capturedInputs: CapturedInputs[] = [];
  private _groupId = uuid();

  private _startTime = 0;
  private _timeLimit = 0;
  private _endTime = 0;
  private _scoreMax = 0;
  private _vr3Queue = [];
  private _vr3 = null;
  private _scoreQueue = [1, 5, 2, 2, 10, 1, 25, 3, 5, 1, 10, 3, 1, 4, 5, 7, 2, 2, 3, 8];
  private _currentPhase = null;

  gameEnded = false;

  constructor(private _router: Router, private _route: ActivatedRoute) { }

  @HostListener('window:keydown', ['$event'])
  onKeyDown(event) {
    if (event.keyCode == 32 && !this._spacePress && !this.gameEnded) {
      this._checkScore();
      this._spacePress = true;

      console.log('Correct responses: ' + this._capturedInputs.filter(x => x.isCorrect).length);
      console.log('Total score: ' + this._score);
    }
  }

  @HostListener('window:keyup', ['$event'])
  onKeyUp(event) {
    if (event.keyCode == 32) {
      this._spacePress = false;
    }
  }

  ngOnInit() {
    this._setRules();
    const canvas = document.getElementById('game') as HTMLCanvasElement;

    this._renderer = new PIXI.Renderer({
      view: canvas,
      width: this._w,
      height: this._h,
      resolution: window.devicePixelRatio,
      autoDensity: true,
      backgroundColor: 0xEEEEEE
    });

    this._stage = new PIXI.Container();
    const ticker = new PIXI.Ticker();

    this._createStrip();

    Array.from(Array(20)).forEach(x => {
      this._createMovingRect();
    });

    this._drawArrow();

    ticker.add(this._loop);
    ticker.start();

  }

  private _setRules() {
    this._sessionStartTime = new Date().getTime();
    this._userId = this._route.snapshot.queryParamMap.get('p');

    setTimeout(() => {
      let user = AppState.state.value.users[this._userId];
      if (user) {
        this._phaseSequence = AppState.state.value.users[this._userId].procedures.replace(/ /g, '').toLowerCase().split(/,/);
        this.nextPhase();
      }
      else {
        alert('Unable to determine participant!');
        this.endGame();
      }
    }, 1000)



  }

  private nextPhase() {
    if (this._currentPhase) {
      this._phaseEndTime = new Date().getTime();
      let captured = this._capturedInputs[this._capturedInputs.length - 1];

      if (captured)
        captured.phaseEndTime = this._phaseEndTime;

      if (this._phaseSequence.length == 1)
        this._sessionEndTime = new Date().getTime();

      this._saveResults();
    }

    this._currentPhase = this._phaseSequence.shift();
    this._resetScore();

    this._phaseStartTime = new Date().getTime();


    console.log(this._currentPhase);

    if (this._currentPhase) {
      switch (this._currentPhase) {
        case 'a': {
          this._startTime = new Date().getTime();
          this._timeLimit = 1000 * 60 * 5;
          this._scoreMax = 30;
          this._scoreModifier = 5;
          this._endTime = this._startTime + this._timeLimit;
          break;
        }
        case 'b': {
          this._vr3Queue = [2, 8, 1, 3, 4, 2, 2, 1, 2, 5, 4, 3, 2, 5, 7, 1, 1, 2, 3, 2];
          this._scoreModifier = 5;
          this._scoreMax = 100
          let randomIndex = Math.floor(Math.random() * this._vr3Queue.length);
          this._vr3 = this._vr3Queue.splice(randomIndex, 1);
          break;
        }
        case 'c': {
          this._vr3Queue = [2, 8, 1, 3, 4, 2, 2, 1, 2, 5, 4, 3, 2, 5, 7, 1, 1, 2, 3, 2];
          this._scoreModifier = 5;
          this._scoreMax = 100
          let randomIndex = Math.floor(Math.random() * this._vr3Queue.length);
          this._vr3 = this._vr3Queue.splice(randomIndex, 1);
          break;
        }
        case 'd': {
          break;
        }
        default: {
          alert('Unable to determine next phase!')
          this._router.navigate(['/']);
          break;
        }
      }
    } else {
      this.endGame();
    }
  }

  private _saveResults() {
    AppState.updateUserResults(this._userId, {
      caputredInputs: this._capturedInputs,
      endTime: this._sessionEndTime,
      startTime: this._sessionStartTime,
      userId: this._userId
    });
  }

  private _resetScore() {
    this._timeLimit = 0;
    this._scoreMax = 0;
    this._scoreModifier = 1;
    this._endTime = 0;
    this._score = 0;
    this.gameEnded = false;
    this._isPaused = false;
    this._capturedInputs = [];
    this._phaseStartTime = null;
    this._phaseEndTime = null;
    this._groupId = uuid();
  }

  private _createStrip() {
    let startY = this._h / 2 - this._stripHeight / 2;

    this._strip = new PIXI.Graphics();

    // Rectangle
    this._strip.beginFill(0x000);
    this._strip.drawRect(0, startY, this._w, this._stripHeight);
    // this._strip.drawRect(100, 100, 100, 100);
    this._strip.endFill();

    // Opt-in to interactivity
    this._strip.interactive = true;

    // Shows hand cursor
    this._strip.buttonMode = true;

    this._stage.addChild(this._strip);
  }

  private _createMovingRect() {
    let startY = this._h / 2 - this._stripHeight / 2;

    let newRect = new MovingRect();

    // Rectangle
    newRect.beginFill(0x8BC34A);
    newRect.drawRect(0, startY, 100, this._stripHeight);
    // this._strip.drawRect(100, 100, 100, 100);
    newRect.endFill();

    // Opt-in to interactivity
    newRect.interactive = true;

    // Shows hand cursor
    newRect.buttonMode = true;

    newRect.width = Math.floor(Math.random() * this._movingRectMaxWidth) + 25;
    newRect.x = newRect.x - newRect.width;

    this._stage.addChild(newRect);
    this._rectQueue.push(newRect);
  }

  private _drawArrow() {
    let startingY = this._h / 2 + this._stripHeight / 2;
    let startingX = this._w / 2;


    this._arrow = new PIXI.Graphics();

    // draw a triangle
    this._arrow.beginFill(0xFF3300);
    this._arrow.moveTo(startingX, startingY);
    this._arrow.lineTo(startingX - 50, startingY + 50);
    this._arrow.lineTo(startingX - 25, startingY + 50);
    this._arrow.lineTo(startingX - 25, startingY + 100);
    this._arrow.lineTo(startingX + 25, startingY + 100);
    this._arrow.lineTo(startingX + 25, startingY + 50);
    this._arrow.lineTo(startingX + 50, startingY + 50);
    this._arrow.lineTo(startingX, startingY);
    this._arrow.endFill();

    // Opt-in to interactivity
    // this._arrow.interactive = true;

    // Shows hand cursor
    // this._arrow.buttonMode = true;

    this._stage.addChild(this._arrow);
  }

  private _checkScore() {
    let hit = false;
    this._movingRects.forEach(rect => {
      if (rect.hasBeenClicked) return;

      let rectBounds = rect.getBounds();
      let arrow = this._arrow.getBounds();
      let arrowPointX = arrow.x + arrow.width / 2;
      if (rectBounds.x <= arrowPointX && arrowPointX <= rectBounds.x + rectBounds.width) {

        hit = true;

        this._isPaused = true;
        rect.hasBeenClicked = true;

        switch (this._currentPhase) {
          case 'a': {
            // let adjustedScore = Math.ceil(this._scoreModifier * (rectBounds.width / this._movingRectMaxWidth));
            // adjustedScore = adjustedScore > this._scoreModifier ? this._scoreModifier : adjustedScore;

            let adjustedScore = 1;
            this._score += adjustedScore;

            this._capturedInputs.push({
              capturedTime: new Date().getTime(),
              isCorrect: true,
              score: adjustedScore,
              phase: this._currentPhase,
              phaseStartTime: this._phaseStartTime,
              phaseEndTime: this._phaseEndTime,
              groupId: this._groupId
            });

            if (this._score >= this._scoreMax) {
              if (this._skipEndGame()) {
                setTimeout(() => {
                  this.nextPhase();
                }, 750)
              } else {
                this.gameEnded = true;
              }
            }

            this._drawScore(rectBounds, adjustedScore);
            break;
          }
          case 'b': {
            this._vr3 -= 1;
            if (this._vr3 == 0) {
              let adjustedScore = 5;
              this._score += adjustedScore;
              this._drawScore(rectBounds, adjustedScore);

              this._capturedInputs.push({
                capturedTime: new Date().getTime(),
                isCorrect: true,
                score: adjustedScore,
                phase: this._currentPhase,
                phaseStartTime: this._phaseStartTime,
                phaseEndTime: this._phaseEndTime,
                groupId: this._groupId
              });

              console.log(this._vr3);
              console.log(this._capturedInputs.filter(x => x.isCorrect));

              if (this._vr3Queue.length == 0) {
                if (this._skipEndGame()) {
                  setTimeout(() => {
                    this.nextPhase();
                  }, 750)
                } else {
                  this.gameEnded = true;
                }
              } else {
                let randomIndex = Math.floor(Math.random() * this._vr3Queue.length);
                this._vr3 = this._vr3Queue.splice(randomIndex, 1);
              }

            }
            break;
          }
          case 'c': {
            this._vr3 -= 1;
            if (this._vr3 == 0) {
              let scoreIndex = Math.floor(Math.random() * this._scoreQueue.length);
              let adjustedScore = this._scoreQueue.splice(scoreIndex, 1)[0];

              this._score += adjustedScore;

              this._drawScore(rectBounds, adjustedScore);

              this._capturedInputs.push({
                capturedTime: new Date().getTime(),
                isCorrect: true,
                score: adjustedScore,
                phase: this._currentPhase,
                phaseStartTime: this._phaseStartTime,
                phaseEndTime: this._phaseEndTime,
                groupId: this._groupId
              });

              console.log(this._vr3);
              console.log(this._vr3Queue.length);
              console.log(this._capturedInputs.filter(x => x.isCorrect));

              if (this._vr3Queue.length == 0) {
                if (this._skipEndGame()) {
                  setTimeout(() => {
                    this.nextPhase();
                  }, 750)
                } else {
                  this.gameEnded = true;
                }
              } else {
                let randomIndex = Math.floor(Math.random() * this._vr3Queue.length);
                this._vr3 = this._vr3Queue.splice(randomIndex, 1);
              }
            }

            break;
          }
          case 'd': {
            let adjustedScore = 0;
            this._score += adjustedScore;

            this._capturedInputs.push({
              capturedTime: new Date().getTime(),
              isCorrect: true,
              score: adjustedScore,
              phase: this._currentPhase,
              phaseStartTime: this._phaseStartTime,
              phaseEndTime: this._phaseEndTime,
              groupId: this._groupId
            });

            break;
          }

          default:
            break;
        }

        setTimeout(() => {
          this._isPaused = false;
        }, 750);
      }
    });

    if (!hit) {
      this._capturedInputs.push({
        capturedTime: new Date().getTime(),
        isCorrect: false,
        score: 0,
        phase: this._currentPhase,
        phaseStartTime: this._phaseStartTime,
        phaseEndTime: this._phaseEndTime,
        groupId: this._groupId
      });
    }
  }

  private _skipEndGame() {
    return this._phaseSequence[0] == 'd';
  }

  private _loop = () => {
    this._renderer.render(this._stage);

    if (this._isPaused || this.gameEnded) return;

    if (this._timeLimit != 0 && new Date().getTime() > this._endTime) {
      this.gameEnded = true;
    }

    // add to moving rects
    if (this._movingRects.length == 0 || (this._rectQueue.length != 0 && this._movingRects[0].x > this._rectDistance)) {
      let nextIndex = Math.floor(Math.random() * (this._rectQueue.length - 1)) + 0;

      this._movingRects.unshift(this._rectQueue[nextIndex]);
      this._rectQueue.splice(nextIndex, 1);

      this._rectDistance = Math.floor(Math.random() * 250) + 25;
    }

    // remove from moving rects
    if (this._movingRects.length > 0) {
      this._movingRects.forEach(rect => rect.x += 5);

      let lastItem = this._movingRects[this._movingRects.length - 1];
      if (lastItem.x > this._w) {
        lastItem.hasBeenClicked = false;
        lastItem.x = 0 - lastItem.width;

        this._rectQueue.push(lastItem);
        this._movingRects.pop();
      }
    }
  }

  private _drawScore(rectBounds: PIXI.Rectangle, adjustedScore: number) {
    let scoreX = (rectBounds.x + rectBounds.width / 2);
    let scoreY = (rectBounds.y + rectBounds.height / 2);

    let star = new PIXI.Graphics();

    star.lineStyle(4, 0x000000);
    star.beginFill(0xFFFFFF, 1);
    star.drawStar(scoreX, scoreY, 5, 50);
    star.endFill();

    const style = new PIXI.TextStyle({
      fontFamily: 'Open Sans',
      fontSize: 24,
      fill: 0xFF0000,
      dropShadow: true,
      dropShadowColor: '#cccccc',
      dropShadowBlur: 4,
      dropShadowAngle: Math.PI / 6,
      dropShadowDistance: 6,
      wordWrap: true,
      wordWrapWidth: 440
    });

    this._scoreText = new PIXI.Text(`${adjustedScore}`, style);
    this._scoreText.x = scoreX - 5;
    this._scoreText.y = scoreY - 12;

    star.addChild(this._scoreText);
    this._stage.addChild(star);

    setTimeout(() => {
      this._stage.removeChild(star);
      this._stage.removeChild(this._stage);
    }, 750)
  }

  endGame() {

    this._router.navigate(['/'])
  }

  manualEndNow() {
    this.gameEnded = true;
    this._isPaused = true;
  }

}

export interface CapturedInputs {
  isCorrect: boolean;
  capturedTime: number;
  score: number;
  phase: string;
  phaseStartTime: number,
  phaseEndTime: number,
  groupId: string
}
